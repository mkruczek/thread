package pl.sdacademy.watki;

import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    TextView textView;
    Button button;
    ProgressBar progressBar;

    Message wiadomosc;
    Handler handler;
    Runnable task;
    long sleepTime;
    Random random;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sleepTime = 10000;
        textView = (TextView) findViewById(R.id.textView);
        button = (Button) findViewById(R.id.button);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar.setMax( (int)(sleepTime/1000) );
        progressBar.setVisibility(View.INVISIBLE);
        final int[] mProgres = {0};

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressBar.setProgress(0);
                mProgres[0] = 0;
                progressBar.setVisibility(View.VISIBLE);

                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mProgres[0]++;
                        progressBar.setProgress(mProgres[0]);
                        if (mProgres[0] < 10) {
                            handler.postDelayed(this, 1000);
                        }
                    }
                }, 1000);


                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        textView.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                random = new Random();
                                int randowmowy = random.nextInt(100);
                                textView.setText(String.valueOf(randowmowy));
                                progressBar.setVisibility(View.INVISIBLE);

                            }
                        }, sleepTime);

                    }
                }).start();


            }
        });


    }
}

